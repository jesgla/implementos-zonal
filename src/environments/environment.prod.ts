export const environment = {
  
  production: true,
  endPoint:'http://replicacion.implementos.cl/wsOmnichannel/Capa_jsonServices',
  endPointB2B :'https://b2b-api.implementos.cl',
  cliente:{
    methods:{
      listadoCliente:'/api/movil/obtenerClientes',
      listadoEvento: '/getListadoEventos.ashx?strrefEvento&strTipoEvento=VISITA&strRutVendedor=15221912-1&strRutCliente&strRutEmisor&strEstado&strDesde=20190201&strHasta=20190402',
      listadoPedido: '/getPedidosCliente.ashx?rutVendedor=15749518-6&rutCliente=76334500-9',
      listadoFlota:'/getFlotaCliente.ashx?rutCliente=16478666-8',
      listadoDireccion: '/getDireccionesAgenda.ashx?rutCliente=76334500-9',
      listadoAgenda : '/getContactosAgenda.ashx?rutVendedor=15221912-1&rutCliente=76334500-9',
      formaPago : '/api/movil/formaPago'
    }
  },
  vendedor:{
    methods:{
      objetivos:'/getResumenVendedor.ashx?rutVendedor=15221912-1&fechaResumen=20180201',
      objetivosAppery:'https://api.appery.io/rest/1/code/25a58147-4d0d-4ff7-b639-596211da1925/exec?rutVendedor=15749518-6&fechaResumen=20180201',
      obtenerVendedores:'/api/movil/obtenerVendedores'
    }
  },
  Evento:{
    methods:{
      crearEvento:'/api/movil/crearEvento',
      RegistrarVisita:'/api/movil/registrarVisita'
    }
  },
  Articulos:{
    methods:{
      obtenerArticulos:'/api/movil/busquedaProductos'
    }
  }
};
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ZonalRoutingModule } from './zonal-routing.module';
import { InformeTiendasComponent } from './pages/informe-tiendas/informe-tiendas.component';
import { TablaTiendaComponent } from './components/tabla-tienda/tabla-tienda.component';
import { InformeZonalComponent } from './pages/informe-zonal/informe-zonal.component';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';


@NgModule({
  declarations: [
    //pages
    InformeTiendasComponent,
    InformeZonalComponent,
    //components
    TablaTiendaComponent
  ],
  imports: [
    FormsModule,
    IonicModule,
    CommonModule,
    ZonalRoutingModule
  ],
  exports:[
    InformeTiendasComponent,
    InformeZonalComponent,
  ]
})
export class ZonalModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InformeZonalComponent } from './informe-zonal.component';

describe('InformeZonalComponent', () => {
  let component: InformeZonalComponent;
  let fixture: ComponentFixture<InformeZonalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformeZonalComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InformeZonalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

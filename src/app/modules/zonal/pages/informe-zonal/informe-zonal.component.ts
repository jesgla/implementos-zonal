import { Component, OnInit, ViewChild } from '@angular/core';
import { Objetivos } from 'src/app/shared/interfaces/vendedor/objetivos';
import { IonSlides, LoadingController } from '@ionic/angular';
import { AutentificacionService } from 'src/app/shared/services/autentificacion/autentificacion.service';
import { SupervisorService } from 'src/app/shared/services/supervisor/supervisor.service';

@Component({
  selector: 'app-informe-zonal',
  templateUrl: './informe-zonal.component.html',
  styleUrls: ['./informe-zonal.component.scss'],
})
export class InformeZonalComponent implements OnInit {
  listaObjetivos: Objetivos[];
  /**
   *elemento slider
   * @type {IonSlides}
   * @memberof DetalleClientePage
   */
  @ViewChild(IonSlides, { static: false }) slidesAdmin: IonSlides;

  /**
   *color de elemento
   * @type {*}
   * @memberof DetalleClientePage
   */
  color: any;

  /**
   *arreglo de paginas de slider
   * @memberof DetalleClientePage
   */
  paginas = [
    { svg: './assets/svg/ubicacion.svg', index: 0, cheked: true },
    { svg: './assets/svg/tiendas.svg', index: 1, disabled: true, cheked: false },
    { svg: './assets/svg/canal.svg', index: 2, disabled: true, cheked: false },
  ];

  /**
   *configuracion de slider
   * @memberof DetalleClientePage
   */
  slideOpts = {
    initialSlide: 1,
    speed: 400
  };
  indice: number;
  totalVenta: number;
  totalMeta: number;
  promedio: number;
  sucursales: Objetivos[];
  canales: Objetivos[];
  vendedores: Objetivos[];
  totalVentaSuc: number;
  totalMetaSuc: number;
  promedioSuc: number;
  totalVentaCan: number;
  totalMetaCan: number;
  promedioCan: number;
  promedioVen: number;
  totalMetaVen: number;
  totalVentaVen: number;
  constructor(
    private authService: AutentificacionService,
    private supervisorService: SupervisorService,
    public loadingController: LoadingController, ) {
    this.indice = 1;
    this.totalVenta = 0;
    this.totalMeta = 0;
    this.promedio = 0;
    this.totalVentaSuc = 0
    this.totalMetaSuc = 0
    this.promedioSuc = 0
    this.totalVentaCan = 0
    this.totalMetaCan = 0
    this.promedioCan = 0
    this.totalVentaVen = 0
    this.totalMetaVen = 0
    this.promedioVen = 0

  }
  ngOnInit(){
    
  }
  /**
      *metodo que se ejecuta al llamar el componente
      * @memberof ObjetivosTabPage
      */
  async ionViewWillEnter() {
    const loading = await this.loadingController.create({
      message: 'Cargando...'
    });
    console.log('datos');
    await loading.present();
    this.totalVenta = 0;
    this.totalMeta = 0;
    this.promedio = 0;
    this.listaObjetivos = await this.supervisorService.obtenerZonas('', 'ZONA');
    console.log('object',this.listaObjetivos);
    this.listaObjetivos.map(async (zona) => {
      let porcentaje = Math.trunc(zona.Venta / zona.MetaAcumulada * 100);
      zona.porcentaje = 0;
      console.log('Infinity', porcentaje);
      if (isFinite(porcentaje) && !isNaN(porcentaje)) {
        
        zona.porcentaje = porcentaje;
      }
      this.totalVenta += zona.Venta;
      this.totalMeta += zona.MetaAcumulada;
      let promedio = this.totalVenta / this.totalMeta * 100;
      console.log('promedio',promedio);
      console.log('promedio',isFinite(promedio));
      console.log('promedio',isNaN(promedio));
      this.promedio = (isFinite(promedio) && !isNaN(promedio)) ?promedio:0 ;
    })
    await loading.dismiss();
    console.log('datos', this.listaObjetivos);
  }
  /* async ngOnInit() {
    
  } */


  async habilitar(indice, slide, zona, tipo) {

    switch (tipo) {
      case 'SUCURSAL':
        this.sucursales = await this.supervisorService.obtenerZonas(zona.nombreVendedor, tipo)
        this.sucursales.map(async (zona) => {
          let porcentaje = Math.trunc(zona.Venta / zona.MetaAcumulada * 100);

          zona.porcentaje = 0;
          if (isFinite(porcentaje) && !isNaN(porcentaje)) {
           
            zona.porcentaje = porcentaje;
          }
          this.totalVentaSuc += zona.Venta;
          this.totalMetaSuc += zona.MetaAcumulada;
          let promedio = this.totalVentaSuc / this.totalMetaSuc * 100;
          console.log('promedio',promedio);
          this.promedioSuc = (isFinite(promedio) && !isNaN(promedio)) ?promedio:0 ;
        })
        break;
      case 'CANAL':
        this.canales = await this.supervisorService.obtenerZonas(zona.nombreVendedor, tipo)
        this.canales.map(async (zona) => {
          let porcentaje = Math.trunc(zona.Venta / zona.MetaAcumulada * 100);

          zona.porcentaje = 0;
          if (isFinite(porcentaje) && !isNaN(porcentaje)) {
            console.log('Infinity', porcentaje);
            zona.porcentaje = porcentaje;
          }

          this.totalVentaCan += zona.Venta;
          this.totalMetaCan += zona.MetaAcumulada;
          let promedio = this.totalVentaCan / this.totalMetaCan * 100;
          console.log('promedio',promedio);
          this.promedioCan = (isFinite(promedio) && !isNaN(promedio)) ?promedio:0 ;
        })
        break;
      case 'VENDEDOR':
        this.vendedores = await this.supervisorService.obtenerZonas(zona.nombreVendedor, tipo);
        this.vendedores.map(async (zona) => {
          let porcentaje = Math.trunc(zona.Venta / zona.MetaAcumulada * 100);

          zona.porcentaje = 0;
          if (isFinite(porcentaje) || isNaN(porcentaje)) {
            console.log('Infinity', porcentaje);
            zona.porcentaje = porcentaje;
          }

          this.totalVentaVen += zona.Venta;
          this.totalMetaVen += zona.MetaAcumulada;

          let promedio = this.totalVentaVen / this.totalMetaVen * 100;
          this.promedioVen = isFinite(promedio) && isNaN(promedio) ? promedio : 0;
        })
        break;




    }

    this.paginas[indice].disabled = false;
    this.paginas[indice].cheked = true;
    let index = this.paginas[indice].index;
    this.paginas.map(async (pag) => {
      pag.index != index ? pag.cheked = false : null
    })
    slide.lockSwipes(false);
    slide.slideTo(indice);
    slide.lockSwipeToNext(true);
    slide.lockSwipes(true);
  }

  validarPorcentaje(valor) {

  }
  /**
    *metodo que se ejecuta despues de iniciar la vista
    * @memberof DetalleClientePage
    */
  ngAfterViewInit() {
    this.slidesAdmin.lockSwipes(true);

  }

  /**
   *permite cambiar la vista de slider
   * @param {*} event
   * @param {*} slide
   * @memberof DetalleClientePage
   */
  cambioPagina(event, slide) {
    slide.lockSwipes(false);
    slide.slideTo(event.detail.value.index);
    slide.lockSwipes(true);

  }
  segmentButtonClicked(ev: any) {
    console.log('Segment button clicked', ev);
  }
  cerrarSesion() {
    this.authService.logout();
  }
}



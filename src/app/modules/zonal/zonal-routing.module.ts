import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InformeZonalComponent } from './pages/informe-zonal/informe-zonal.component';


const routes: Routes = [
  {
    path: '',
    component: InformeZonalComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ZonalRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { VendedorService } from 'src/app/shared/services/vendedor/vendedor.service';
import { DataLocalService } from 'src/app/shared/services/dataLocal/data-local.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-vendedores',
  templateUrl: './vendedores.component.html',
  styleUrls: ['./vendedores.component.scss'],
})
export class VendedoresComponent implements OnInit {

 
  lstVendedores : any [];
  obtener: boolean;
  textoBuscar: string;
  constructor(
    private vendedorService : VendedorService,
     private dataLocal : DataLocalService,
     private router : Router) { 
    this.lstVendedores =[];
    this.textoBuscar='';
  }
  ngOnInit(){
    console.log('vendedores');
  }
 /**
   *Metodo que se ejecuta cada vez que se llama el componente
   *
   * @memberof ClientesTabPage
   */
  async ionViewWillEnter() {
 
    this.obtener = true;
    
    this.lstVendedores = await this.vendedorService.obtenerVendedores();
    this.lstVendedores.map(vt=>{
      let arr = vt.nombreCompleto.split(' ');
      vt.nombreCompleto = `${arr[2]} ${arr[3]?arr[3]:''} ${arr[0]} ${arr[1]} `;
    })
    this.obtener = false;

  }
  async verResumen(vendedor){
    await this.dataLocal.setItem('vendedor',vendedor);
    await this.router.navigate(['detalle-vendedor'])
  }
  /**
   *permite buscar cliente 
   * @param {*} event
   * @memberof ClientesPage
   */
  buscar( event ) {
    this.textoBuscar = event.detail.value;
  }
}
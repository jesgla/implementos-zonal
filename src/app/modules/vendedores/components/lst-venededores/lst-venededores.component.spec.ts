import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LstVenededoresComponent } from './lst-venededores.component';

describe('LstVenededoresComponent', () => {
  let component: LstVenededoresComponent;
  let fixture: ComponentFixture<LstVenededoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LstVenededoresComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LstVenededoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

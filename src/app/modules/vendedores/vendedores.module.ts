import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VendedoresRoutingModule } from './vendedores-routing.module';
import { VendedoresComponent } from './pages/vendedores/vendedores.component';
import { LstVenededoresComponent } from './components/lst-venededores/lst-venededores.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    VendedoresComponent,
    LstVenededoresComponent
  ],
  imports: [
    FormsModule,
    IonicModule,
    CommonModule,
    VendedoresRoutingModule,
    SharedModule
  ],
  exports:[VendedoresComponent]
})
export class VendedoresModule { }

import { Component, OnInit } from '@angular/core';
import { DataLocalService } from 'src/app/shared/services/dataLocal/data-local.service';
import { ArticulosService } from 'src/app/shared/services/articulos/articulos.service';
import { ModalController, AlertController, NavController } from '@ionic/angular';
import { Articulo } from 'src/app/shared/interfaces/articulo/articulo';
import { Vendedor } from 'src/app/shared/interfaces/vendedor/vendedor';



@Component({
  selector: 'app-articulos',
  templateUrl: './articulos.component.html',
  styleUrls: ['./articulos.component.scss'],
})
export class ArticulosComponent implements OnInit {
 /**
   * variable que permite saber si obtuvo el listado de articulos
   * @type {boolean}
   * @memberof ArticulosPage
   */
  obtener: boolean;

  /**
   * Listado de articulos 
   * @type {Articulo}
   * @memberof ArticulosPage
   */
  listadoArticulo: Articulo;

  /**
   * Objeto de vendedor 
   * @type {Vendedor}
   * @memberof ArticulosPage
   */
  vendedor: Vendedor;
  items: number =0;

  /**
   *Creates an instance of ArticulosPage.
   * @param {ArticulosService} articuloService
   * @param {ModalController} modalController
   * @param {AlertController} alertController
   * @param {NavController} navCtrl
   * @param {DataLocalService} dataLocal
   * @memberof ArticulosPage
   */
  constructor(
    private articuloService: ArticulosService,
    public modalController: ModalController,
    public alertController: AlertController,
    private navCtrl: NavController,
    private dataLocal: DataLocalService) {
    this.obtener = false;
  }

  /**
   * Permite ejecutar funciones al iniciar el componente.
   * @memberof ArticulosPage
   */
  async ngOnInit() {
    //await this.presentAlert();
    await this.obtenerUsuario();

    this.obtener = true;

    let bodega = this.vendedor.codBodega == "" || this.vendedor.codBodega == "AREA ADMINISTRACION" ? "CDD-CD1" : this.vendedor.codBodega;

    let consulta = await this.articuloService.obtenerArticulos({
      "descripcion": "cinta",
      "sucursal": bodega
    });

    this.obtener = false;
    this.listadoArticulo = consulta.items;
  }
   async ionViewWillEnter() {

    await this.obtenerCarro();
  }

  /**
   * Permite obtener usuario logueado en la aplicacion
   * @memberof ArticulosPage
   */
  async obtenerUsuario() {
    let usuario = await this.dataLocal.getItem('auth-token');
    this.vendedor = JSON.parse(usuario);

  }

  /**
   * Permite buscar articulos ingresando su SKU
   * @param {string} articulo
   * @memberof ArticulosPage
   */
  async buscar(articulo: string) {
    this.obtener = true;
    let bodega = this.vendedor.codBodega == "" || this.vendedor.codBodega == "AREA ADMINISTRACION" ? "CDD-CD1" : this.vendedor.codBodega;
    let parametros = {
      "descripcion": articulo,
      "sucursal": bodega
    }

    let consulta = await this.articuloService.obtenerArticulos(parametros);
    this.obtener = false;

    this.listadoArticulo = consulta.items;
  }

  /**
   * Permite abrir modal para visualizar un detalle del articulo seleccionado
   * @param {*} articulo
   * @returns modal detalle articulos
   * @memberof ArticulosPage
   */
  async abrirModal(articulo) {
    const modal = await this.modalController.create({
      component: 'DetalleArticuloPage',
      componentProps: {
        articulo,
        boton: false,
        cantidad: 1,
      }
    },
    );
 
    await modal.present();
    const { data } = await modal.onWillDismiss();

    data.recargar?await this.obtenerCarro():null;
  }

  async obtenerCarro() {

    let carro = await this.dataLocal.getCarroCompra();


    this.items = carro.length; 
  
    
  }
}
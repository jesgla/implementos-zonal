import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArticulosRoutingModule } from './articulos-routing.module';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ArticulosComponent } from './pages/articulos/articulos.component';
import { LstArticulosComponent } from './components/lst-articulos/lst-articulos.component';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    ArticulosComponent,
    LstArticulosComponent
  ],
  imports: [
    FormsModule,
    IonicModule,
    CommonModule,
    ArticulosRoutingModule,
    SharedModule
  ],

  exports: [
    ArticulosComponent,
    LstArticulosComponent
  ]
})
export class ArticulosModule { }

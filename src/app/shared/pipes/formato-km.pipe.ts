//angular
import { Pipe, PipeTransform } from '@angular/core';
/**
 *Permite setear los miles en kilometros
 * @export
 * @class FormatoKmPipe
 * @implements {PipeTransform}
 */
@Pipe({
  name: 'formatoKm'
})

export class FormatoKmPipe implements PipeTransform {
  /**
   *Funcion que separa en miles los valores enviados
   * @param {*} value
   * @returns {*} valor en miles
   * @memberof FormatoKmPipe
   */
  transform(value: any): any {
    if (!value) {
      return '0 km';
    }
    value = value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    value = `${value} km`;
    return value;
  }

}

/**
 *
 *
 * @export
 * @ignore
 * @interface Objetivos
 */
export interface Objetivos {
  codVendedor: string;
  rutVendedor: string;
  nombreVendedor?: any;
  fecha: string;
  tipo: string;
  Venta: number;
  VentaDia: number;
  Meta: number;
  MetaAcumulada: number;
  MetaDia: number;
  CotizacionesCreadas: number;
  CotizacionesConfirmadas: number;
  PedidosCreados: number;
  PedidosConfirmados: number;
  VisitasCreadas: number;
  VisitasConfirmadas: number;
  cantClientes: number;
  ClientescVta: number;
  ClientescVisita: number;
  fonoVendedor?: any;
  porcentaje : number;
}
/**
 *
 *
 * @ignore
 * @interface FormaPago
 */
interface FormaPago {
  codTipo: string;
  tipo: string;
}
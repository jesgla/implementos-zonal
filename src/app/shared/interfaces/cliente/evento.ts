/**
 *
 *
 * @export
 * @ignore
 * @interface Evento
 */
export interface Evento {
  fechaCorregida: string;
  idEvento: string;
  referenciaEvento: string;
  rutEmisor: string;
  rutVendedor: string;
  rutCliente: string;
  nombreCliente: string;
  tipoEvento: string;
  fechaCreacion: string;
  fechaEjecucion: string;
  fechaProgramada: string;
  Observaciones: string;
  Estado: string;
  Direccion: string;
  Latitud: string;
  Longitud: string;
}
//angular
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

//ionic
/* import { NativeGeocoder, NativeGeocoderResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx'; */

//variables de entorno
import { environment } from 'src/environments/environment';

//interfaces


//extras
import * as turf from '@turf/turf';
import { Respuesta } from '../../interfaces/cliente/cliente';
import { Evento } from '../../interfaces/cliente/evento';
import { Pedido } from '../../interfaces/cliente/pedido';
import { Flota } from '../../interfaces/cliente/flota';
import { Direccion } from '../../interfaces/direccion/direccion';


@Injectable({
  providedIn: 'root'
})

/**
 *Servicio de cliente
 * @export
 * @class ClienteService
 */
export class ClienteService {
  
  /**
   *Objeto cliente
   * @memberof ClienteService
   */
  cliente = null;

  /**
   *latitud de la geolocalizacion
   * @type {number}
   * @memberof ClienteService
   */
  latitud: number;

  /**
   *longitud de la geolocalizacion
   * @type {number}
   * @memberof ClienteService
   */
  longitud: number;

  /**
   *Creates an instance of ClienteService.
   * @param {HttpClient} httpClient
   * @param {Geolocation} geolocation
   * @param {NativeGeocoder} nativeGeocoder
   * @memberof ClienteService
   */
  constructor(
    private httpClient: HttpClient,
    private geolocation: Geolocation,
    ) { }

  /**
   *header para las peticiones post
   * @memberof ClienteService
   */
  header = new HttpHeaders(
    {
      "Accept": 'application/json',
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
    });

  /**
   *Configuracion de geolicalizacion
   * @type {NativeGeocoderOptions}
   * @memberof ClienteService
   */
  /* options: NativeGeocoderOptions = {
    useLocale: true,
    maxResults: 5
  }; */

  /**
   *Permite obtener los clientes de un vendedor
   * @param {*} vendedor
   * @returns
   * @memberof ClienteService
   */
  async obtenerClientes(vendedor) {
    let consulta = null
    console.log('vendedor',vendedor)
    //const url = `http://localhost:1900/api/movil/listarCO?codigo=${vendedor.codEmpleado}`;
    const url =`https://b2b-api.implementos.cl/api/movil/listarCO?codigo=${vendedor.codEmpleado}` 
    consulta = await this.httpClient.get<Respuesta>(url, { headers: this.header }).toPromise();
    console.log('clientes',consulta.data)
    
    return consulta.data;
  }

  /**
   *Permite devolver un cliente seleccionado
   * @param {*} cliente
   * @returns
   * @memberof ClienteService
   */
  async detalleCliente(cliente) {
    this.cliente = cliente;
    return await this.cliente;
  }

  /**
   *Permite obtener un cliente por su rut
   * @param {*} cliente
   * @returns
   * @memberof ClienteService
   */
  async obtenerCliente(cliente) {
    let consulta = null;
    let parametros = {
      rut: cliente.rutCliente.replace('.', '').replace('.', ''),
      token: cliente.rutCliente

    }
    console.log('si es este',parametros);
    //const url = `https://b2b-api.implementos.cl/api/cliente/GetDatosCliente`;
    const url = `https://b2b-api.implementos.cl/api/movil/GetDatosCliente`;

    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();
    console.log('consulta',consulta);
    return consulta.data;
  }

  /**
   *Permite obtener el listado de eventos por cliente
   * @param {*} vendedor
   * @param {*} cliente
   * @returns listado de eventos
   * @memberof ClienteService
   */
  async obtenerEventos(vendedor, cliente) {
    let consulta = null
    let parametros = {
      rutVendedor: vendedor.rut.replace('.', '').replace('.', ''),
      rutCliente: cliente.rut.replace('.', '').replace('.', '')

    }
 

    const url = `https://b2b-api.implementos.cl/api/movil/listadoEventos`;
    consulta = await this.httpClient.post<Evento>(url, parametros, { headers: this.header }).toPromise();
   
    return consulta;
  }

  /**
   *Permite obtener el listado de pedidos por cliente
   * @param {*} vendedor
   * @param {*} cliente
   * @returns
   * @memberof ClienteService
   */
  async obtenerPedidos(vendedor, cliente) {
    let consulta = null;
    let parametros = {
      rutVendedor: vendedor.rut,
      rutCliente: cliente.rut

    }

    const url = `https://b2b-api.implementos.cl/api/movil/listadoPedidos`;
    consulta = await this.httpClient.post<Pedido>(url, parametros, { headers: this.header }).toPromise();

    return consulta;
  }

  /**
   *Permite obtener la flota de un cliente
   * @param {*} cliente
   * @returns
   * @memberof ClienteService
   */
  async obtenerFlota(cliente) {
    let consulta = null
    let parametros = {
      rutCliente: cliente.rut

    }

    const url = `https://b2b-api.implementos.cl/api/movil/listadoFlota`;
    consulta = await this.httpClient.post<Flota>(url, parametros, { headers: this.header }).toPromise();

    return consulta;
  }

  /**
   *Permite obtener las direcciones de los clientes
   * @returns
   * @memberof ClienteService
   */
  async obtenerDirecciones() {
    let consulta = null
    const url = `${environment.endPoint}${environment.cliente.methods.listadoDireccion}`;
    consulta = await this.httpClient.get<Direccion>(url, { responseType: 'json' }).toPromise();

    return consulta;
  }

  /**
   *Permite obtener la agenda de los clientes
   * @returns
   * @memberof ClienteService
   */
  async obtenerAgenda() {
    let consulta = null
    const url = `${environment.endPoint}${environment.cliente.methods.listadoAgenda}`;
    consulta = await this.httpClient.get<Direccion>(url, { responseType: 'json' }).toPromise();

    return consulta;
  }

  /**
   *Permite obtener la forma de pago de los clientes
   * @param {*} rutCliente
   * @returns
   * @memberof ClienteService
   */
  async obtenerFormaPago(rutCliente) {
    let consulta = null
    const url = `${environment.endPointB2B}${environment.cliente.methods.formaPago}`;

    consulta = await this.httpClient.post(url, { rutCliente }, { headers: this.header }).toPromise();

    return consulta;
  }

  /**
   *Permite crear una cotizacion para un cliente
   *
   * @param {{ codTipoFormaPago: string; rutVendedor: string; rutCliente: string; rutEmisor: string; codSucursal: string; detalle: string; }} parametros
   * @returns
   * @memberof ClienteService
   */
  async crearCotizacion(parametros: { codTipoFormaPago: string; rutVendedor: string; rutCliente: string; rutEmisor: string; codSucursal: string; detalle: string; }) {
    let consulta = null
    const url = `${environment.endPointB2B}${environment.Evento.methods.crearEvento}`;
    //const url = `http://localhost:1900${environment.Evento.methods.crearEvento}`;

    
    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();
  
    return consulta;
  }
  /**
   *Permite registrar el check-in de cliente
   * @param {*} parametros
   * @returns
   * @memberof ClienteService
   */
  async crearVisita(parametros) {

    let consulta = null
    //const url = `http://localhost:1900/api/movil/agendarVisita`;
    const url = `${environment.endPointB2B}/api/movil/agendarVisita`;
    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();

    return consulta;
  }

  /**
   *Permite calcular la distancia entre el vendedor y el cliente
   * @param {*} clientes
   * @returns
   * @memberof ClienteService
   */
  

  async obtenerDetallePedido(parametros) {
    let consulta = null;
    let rut = parametros.rutCliente.replace('.','').replace('.','');
    let ov = parametros.folio;
    const url = `${environment.endPointB2B}/api/movil/detallePedido?rutCliente=${rut}&folio=${ov}`;
   // const url = `http://localhost:1900/api/movil/detallePedido?rutCliente=${rut}&folio=${ov}`;

    
    consulta = await this.httpClient.get(url,{headers:this.header} ).toPromise();
    console.log('consulta',consulta);
    return consulta;
  }
}

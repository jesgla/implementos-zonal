//angular
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

//environment
import { environment } from 'src/environments/environment';


//interfaces
import { Articulo } from '../../interfaces/articulo/articulo';


@Injectable({
  providedIn: 'root'
})

/**
 * Servicio de articulos
 * @export
 * @class ArticulosService
 */
export class ArticulosService {

  /**
   *Creates an instance of ArticulosService.
   * @param {HttpClient} httpClient
   * @memberof ArticulosService
   */
  constructor(private httpClient: HttpClient) { }
  /**
   * header para ejecutar consultas post
   * @memberof ArticulosService
   */
  header = new HttpHeaders(
    {
      "Accept": 'application/json',
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
    });

  /**
   *Permite obtener listado de articulos
   * @param {*} parametros
   * @returns
   * @memberof ArticulosService
   */
  async obtenerArticulos(parametros) {
    let consulta = null;

    const url = `${environment.endPointB2B}${environment.Articulos.methods.obtenerArticulos}`;
    //const url = `http://localhost:1900${environment.Articulos.methods.obtenerArticulos}`;
    //const url =  `${environment.endPointB2B}${environment.Articulos.methods.obtenerArticulos}`;

    console.log('parametros',parametros);
    console.log('url',url);
    consulta = await this.httpClient.post<Articulo>(url, parametros, { headers: this.header }).toPromise();
    console.log('consulta',consulta);
    return consulta;
  }

  /**
   *Permite obtener stock de un producto por almacen
   * @param {*} parametros
   * @returns
   * @memberof ArticulosService
   */
  async obtenerStockTiendas(parametros) {
    let consulta = null;

    //const url =  `http://localhost:1900/api/movil/obtenerStockTiendas`;
    const url = `https://b2b-api.implementos.cl/api/movil/obtenerStockTiendas`;


    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();

    return consulta.stockTiendas;
  }

}

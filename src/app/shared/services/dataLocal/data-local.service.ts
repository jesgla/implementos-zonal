//angular
import { Injectable } from '@angular/core';

//ionic
import { Storage } from '@ionic/storage';
import { ToastController } from '@ionic/angular';
import { Articulo } from '../../interfaces/articulo/articulo';
import { Cliente } from '../../interfaces/cliente/cliente';

//interfaces


@Injectable({
  providedIn: 'root'
})

/**
 *Permite almacenar los datos en local storage
 * @export
 * @class DataLocalService
 */
export class DataLocalService {
  
  /**
   *arreglo de carro de compras
   * @type {Articulo[]}
   * @memberof DataLocalService
   */
  carro : Articulo[]=[];

  /**
   *objeto articulo
   * @type {Articulo}
   * @memberof DataLocalService
   */
  nuevoArticulo: Articulo;

  /**
   *Creates an instance of DataLocalService.
   * @param {Storage} storage
   * @param {ToastController} toastController
   * @memberof DataLocalService
   */
  constructor(private storage: Storage,public toastController: ToastController) { 
    this.storage.set('carro',[]);
    this.storage.set('geo',[]);
    storage.remove('cliente');
  }

  /**
   *Permite almacenar el carro de compras
   * @param {Articulo} articulo
   * @param {boolean} estado
   * @returns
   * @memberof DataLocalService
   */
  async setCarroCompra(articulo:Articulo,estado: boolean){
    this.carro =await this.storage.get('carro');

    let existe = this.carro.find(articuloA => articuloA.sku === articulo.sku);

    if(!existe){
      this.carro.unshift(articulo)
      this.carro = this.carro.filter(art=>art.cantidad>0);
  
      
      this.storage.set('carro',this.carro)
    }else{

      if(estado){
        existe.cantidad+=articulo.cantidad;
      }else{
        existe.cantidad-=articulo.cantidad;
      }
      
      this.carro = this.carro.filter(art=>art.cantidad>0);
      this.presentToast( this.carro.length.toString(),'success' )
  
     
      this.storage.set('carro',this.carro);

    }
    this.presentToast( 'Articulo Agregado','success' )
    return await this.storage.get('carro');
    
  }

  /**
   *Almacena un cliente en ambiente local
   *
   * @param {Cliente} cliente
   * @memberof DataLocalService
   */
  async setCliente(cliente: Cliente) {
    await this.storage.remove('carro');
    await this.storage.remove('referencia');
    this.storage.set('carro',[]);
  
    
    await this.storage.set('cliente',cliente);

  }

  /**
   *Permite consultar por el carro de compras
   * @returns
   * @memberof DataLocalService
   */
  async getCarroCompra(){
    return await this.storage.get('carro');
  }

  /**
   *Permite mostrar un mensaje al usuario
   * @param {string} message
   * @memberof DataLocalService
   */
  async presentToast( message: string,color) {
    const toast = await this.toastController.create({
      message,
      duration: 1500,
      position: 'top',
      color: color
    });
    toast.present();
  }

  /**
   *Permite eliminar un articulo del carro de compra
   * @param {*} sku
   * @memberof DataLocalService
   */
  async eliminarArticulo(sku: any) {
    this.carro =await this.storage.get('carro');
 
    this.carro = this.carro.filter(articulo => articulo.sku!= sku);

    this.storage.set('carro',this.carro);
    this.presentToast( 'Articulo Eliminado','success' )

  }

  /**
   *Permite obtener un cliente
   * @returns
   * @memberof DataLocalService
   */
  async getCliente() {
    return await this.storage.get('cliente');
  }

  /**
   *Permite eliminar el carro de compras
   * @param {string} dato
   * @memberof DataLocalService
   */
  async eliminarDatos(dato: string) {
    
    this.storage.remove(dato);
    if(dato==='carro'){
      this.storage.set('carro',[]);
    }
  }

  /**
   *Almacena la referencia del check-in
   * @param {*} id
   * @memberof DataLocalService
   */
  async setIdReferencia(id) {
    this.storage.set('referencia',id);
    this.presentToast( 'registro check-in exitoso','success' );
  }

  /**
   *Permite obtener un elemento de la local storage
   * @param {string} elemento
   * @returns
   * @memberof DataLocalService
   */
  async getItem(elemento: string){
    return await this.storage.get(elemento);
  }

  /**
   *Permite ordenar la fecha para almacenar 
   * @param {Date} fechaCompleta
   * @param {boolean} tipoFecha
   * @returns
   * @memberof DataLocalService
   */
  ordenarFecha(fechaCompleta: Date,tipoFecha : boolean) {
 
    fechaCompleta = new Date(fechaCompleta);

    let dia  = fechaCompleta.getDate();
    let mes  = fechaCompleta.getMonth()+1;
    let anio = fechaCompleta.getFullYear();
    let hora = fechaCompleta.getHours();
    let min  = fechaCompleta.getMinutes();
    mes = mes>12?1:mes;
    let diaSrt = dia<10?'0'+dia:dia;
    let mesStr = mes<10?'0'+mes:mes;
    let horaStr = hora<10?'0'+hora:hora;
    let minStr = min<10?'0'+min:min;

    let formatoFecha =null;
    if(tipoFecha){
      formatoFecha =`${anio}${mesStr}${diaSrt}`;
    }else{
      formatoFecha =`${anio}${mesStr}${diaSrt}${horaStr}${minStr}`;
    }

    return formatoFecha

  } 

  async setItem(key: string, datos: any) {
    await this.storage.set(key,datos);
  }
}

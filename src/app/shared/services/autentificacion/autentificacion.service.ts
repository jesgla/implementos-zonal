//angular
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

//ionic
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';

//services
import { DataLocalService } from '../dataLocal/data-local.service';

//environment
import { environment } from 'src/environments/environment';
import { Vendedor } from '../../interfaces/vendedor/vendedor';

//iterfaces


/**
 * constante 
 */
const TOKEN_KEY = 'auth-token';
/**
 *Servicio que permite obtener sesion de usuario
 * @export
 * @class AutentificacionService
 */
@Injectable({
  providedIn: 'root'
})


export class AutentificacionService {

  /**
   *validacion de sesion 
   * @memberof AutentificacionService
   */
  authenticationState = new BehaviorSubject(false);

  /**
   *Creates an instance of AutentificacionService.
   * @param {Router} router
   * @param {Storage} storage
   * @param {Platform} plt
   * @param {HttpClient} httpClient
   * @param {DataLocalService} dataLocal
   * @memberof AutentificacionService
   */
  constructor(private router: Router, private storage: Storage, private plt: Platform, private httpClient: HttpClient, private dataLocal: DataLocalService) {
    this.plt.ready().then(() => {
      this.checkToken();
    });
  }

  /**
   *Permite validar si existe usuario logueado
   * @returns objeto de vendedor
   * @memberof AutentificacionService
   */
  async checkToken() {
    let vendedor = await this.storage.get(TOKEN_KEY);

    if (vendedor) {

      this.authenticationState.next(true);
      return JSON.parse(vendedor);
    }
  }

  /**
   *Permite consultar por usuario que quiere iniciar sesion en la aplicacion
   * @param {*} usuario
   * @returns
   * @memberof AutentificacionService
   */
  async login(usuario) {
    let consulta = null;
    const url = `${environment.endPointB2B}/api/movil/loginUsuario`;
    consulta = await this.httpClient.post<Vendedor>(url, usuario).toPromise();
    console.log('login',consulta);
    if (consulta.length > 0) {
      await this.storage.remove(TOKEN_KEY);
      return this.storage.set(TOKEN_KEY, JSON.stringify(consulta[0])).then(() => {
        this.authenticationState.next(true);
      });
    } else {
      this.dataLocal.presentToast('Usuario o contraseña incorrecta','danger');
    }

  }

  /**
   *Permite cerrar sesion en la aplicacion  
   * @returns
   * @memberof AutentificacionService
   */
  async logout() {
    await this.storage.remove(TOKEN_KEY);
    await this.router.navigate(['login']);

    return await this.authenticationState.next(false);
  }

  /**
   *Permite autenticar el usuario logueado
   * @returns
   * @memberof AutentificacionService
   */
  isAuthenticated() {
    return this.authenticationState.value;
  }

}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DataLocalService } from '../dataLocal/data-local.service';

@Injectable({
  providedIn: 'root'
})
export class SupervisorService {

  constructor(private httpClient: HttpClient, private dataLocal: DataLocalService) { }
  /**
     * header para ejecutar consultas post
     * @memberof ArticulosService
     */
  header = new HttpHeaders(
    {
      "Accept": 'application/json',
      'Content-Type': 'application/json; charset=utf-8',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
    });

  async obtenerZonas(sector, tipoSector) {
    let consulta = null;
    const url = `https://b2b-api.implementos.cl/api/movil/obtenerZonas`;
    let usuario =  JSON.parse(await this.obtenerUsuario());
    console.log(usuario);
    let parametros = {
      "rutEmisor": usuario.rut.replace('.', '').replace('.', ''),
      "fechaResumen": this.formatoFecha(),
      "sector": sector,
      "tipoSector": tipoSector,
      "token": "new-app"

    }
    console.log('obtenerZonas', parametros)

    consulta = await this.httpClient.post(url, parametros, { headers:this.header }).toPromise();
    console.log('obtenerZonas', consulta)
    return consulta;
  }

  async obtenerUsuario() {
    let usuario = await this.dataLocal.getItem('auth-token');

    return usuario;
  }

  /**
 *Permite ordenar la fecha para almacenar
 * @returns fecha
 * @memberof VendedorService
 */
  formatoFecha() {
    let fecha = new Date();
    let dia = fecha.getDate();
    let mes = fecha.getMonth() + 1;
    let anio = fecha.getFullYear();
    let hora = fecha.getHours();
    let min = fecha.getMinutes();
    let seg = fecha.getSeconds();
    let diaSrt = dia < 10 ? '0' + dia : dia;
    let mesStr = mes < 10 ? '0' + mes : mes;
    let horaStr = hora < 10 ? '0' + hora : hora;
    let minStr = min < 10 ? '0' + min : min;
    let segStr = seg < 10 ? '0' + seg : seg;

    let formatoFecha = `${anio}${mesStr}${diaSrt}`;

    return formatoFecha
  }
}

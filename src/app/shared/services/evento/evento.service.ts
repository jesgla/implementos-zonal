import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';



import { DataLocalService } from '../dataLocal/data-local.service';
import { Evento } from '../../interfaces/evento/evento';

@Injectable({
  providedIn: 'root'
})

/**
 *Servicio de eventos
 * @export
 * @class EventoService
 */
export class EventoService {
  /**
   *objeto de evento
   * @type {Evento}
   * @memberof EventoService
   */
  crearEvento: Evento;

  /**
   *Creates an instance of EventoService.
   * @param {HttpClient} httpClient
   * @param {DataLocalService} dataLocal
   * @memberof EventoService
   */
  constructor(
    private httpClient: HttpClient,
    private dataLocal: DataLocalService
  ) {
    this.crearEvento = new Evento();
  }

  /**
   *header para peticiones post
   * @memberof EventoService
   */
  header = new HttpHeaders(
    {
      "Accept": 'application/json',
      'Content-Type': 'application/json',
      'response-Type': 'json'
    }

  );

  /**
   *Permite obtener eventos del cliente
   * @param {*} vendedor
   * @returns
   * @memberof EventoService
   */
  async obtenerEventosCliente(vendedor, fechas, estado) {
    let consulta = null;
    console.log('vendedor', vendedor);
    let parametros = {
      strRutVendedor: vendedor.rut.replace('.', '').replace('.', ''),
      //fecha: await this.ordenarFecha(fecha)
      fechaInicio: fechas.inicio,
      fechaTermino: fechas.termino,
      estado: estado
    }
    console.log('parametros', parametros)
    //let url = ` http://localhost:1900/api/movil/obtenerEventos`;
    let url = `${environment.endPointB2B}/api/movil/obtenerEventos`;

    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();

    return consulta;
  }

  /**
   *Permite generar un evento de cliente
   * @param {*} objeto
   * @returns
   * @memberof EventoService
   */
  async generarEvento(objeto) {
    let consulta = null;
    //let url = ` http://localhost:1900/api/movil/agendarVisita`;

    let url = `${environment.endPointB2B}/api/movil/agendarVisita`;
    let parametros = this.setearObjeto(objeto);
    console.log('parametros', parametros);
    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();
    console.log('crear evento', consulta);
    return consulta;
  }

  /**
   *Permite ordenar la fecha para almacenar
   * @param {Date} fechaCompleta
   * @returns
   * @memberof EventoService
   */
  ordenarFecha(fechaCompleta: Date) {

    fechaCompleta = new Date(fechaCompleta);


    let dia = fechaCompleta.getDate();
    let mes = fechaCompleta.getMonth() + 1;
    let anio = fechaCompleta.getFullYear();
    let hora = fechaCompleta.getHours();
    let min = fechaCompleta.getMinutes();
    mes = mes > 12 ? 1 : mes;
    let diaSrt = dia < 10 ? '0' + dia : dia;
    let mesStr = mes < 10 ? '0' + mes : mes;
    let horaStr = hora < 10 ? '0' + hora : hora;
    let minStr = min < 10 ? '0' + min : min;

    let formatoFecha = `${anio}${mesStr}${diaSrt}${horaStr}${minStr}`

    return formatoFecha

  }

  /**
   *Permite llenar objeto de evento
   * @param {*} objeto
   * @returns
   * @memberof EventoService
   */
  setearObjeto(objeto) {

    this.crearEvento = new Evento();
    this.crearEvento.idEvento = objeto.idEvento ? objeto.idEvento : '';
    this.crearEvento.refVisita = objeto.refVisita ? objeto.refVisita : '';
    this.crearEvento.tipoEvento = objeto.tipoEvento ? objeto.tipoEvento : '';
    this.crearEvento.fechaCreacion = objeto.fechaCreacion ? this.ordenarFecha(objeto.fechaCreacion) : '';
    this.crearEvento.fechaEjecucion = objeto.fechaEjecucion ? this.ordenarFecha(objeto.fechaEjecucion) : '';
    this.crearEvento.fechaProgramada = objeto.fechaProgramada ? this.ordenarFecha(objeto.fechaProgramada) : '';
    this.crearEvento.observaciones = objeto.observaciones ? objeto.observaciones : '';
    this.crearEvento.rutEmisor = objeto.rutEmisor ? objeto.rutEmisor : '';
    this.crearEvento.rutVendedor = objeto.rutVendedor ? objeto.rutVendedor : '';
    this.crearEvento.rutCliente = objeto.rutCliente ? objeto.rutCliente : '';
    this.crearEvento.estado = objeto.estado ? objeto.estado : '';
    this.crearEvento.direccion = objeto.direccion ? objeto.direccion : '';
    this.crearEvento.latitud = objeto.latitud ? objeto.latitud : '';
    this.crearEvento.longitud = objeto.longitud ? objeto.longitud : '';
    this.crearEvento.detalle = objeto.detalle ? objeto.detalle : '';
    this.crearEvento.nombreCliente = objeto.nombreCliente ? objeto.nombreCliente : '';
    this.crearEvento.referenciaEvento = objeto.referenciaEvento ? objeto.referenciaEvento : '';


    return this.crearEvento;
  }


}

import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { IonicModule } from '@ionic/angular';

//components
import { CalendarioComponent } from './components/calendario/calendario.component';
import { HeaderComponent } from './components/header/header.component';

//services 
import { EventoService } from './services/evento/evento.service';
import { VendedorService } from './services/vendedor/vendedor.service';
import { DataLocalService } from './services/dataLocal/data-local.service';
import { FormatoMonedaPipe } from './pipes/formato-moneda.pipe';
import { FiltroPipe } from './pipes/filtro.pipe';
import { FormatoKmPipe } from './pipes/formato-km.pipe';


@NgModule({
  declarations: [
    //pipes 
    FormatoMonedaPipe,
    FiltroPipe,
    FormatoKmPipe,

    //components
    CalendarioComponent,
    HeaderComponent,
  
  ],
  imports: [
    CommonModule,
    IonicModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
  
  ],
  providers: [
    VendedorService,
    EventoService,
    DataLocalService,
   /*  {
      provide: HTTP_INTERCEPTORS,
      useClass: BasicAuthInterceptor,
      multi: true
    } */
  ],
  exports:[
    //pipes 
    FormatoMonedaPipe,
    FiltroPipe,
    FormatoKmPipe,

    //components
    CalendarioComponent,
    HeaderComponent,
    
  ]
})
export class SharedModule { }

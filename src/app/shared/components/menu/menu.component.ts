import { Component } from '@angular/core';
/**
 * permite navegar en la aplicación con sus diferentes opciones.
 * @export
 * @class MenuComponent
 */
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})


export class MenuComponent {
  /**
   *Creates an instance of MenuComponent.
   * @memberof MenuComponent
   */
  constructor() { }

  /**
   * Listado de menu de la aplicacion
   * @type {Array} 
   * @memberof MenuComponent
   */
  listaMenu = [
    { icono: 'home', nombre: 'Objetivo', directTo: '/tabs/objetivos' },
    { icono: 'person', nombre: 'Objetivo', directTo: '/tabs/clientes' },
    { icono: 'calendar', nombre: 'Objetivo', directTo: '/tabs/calendario' },
    { icono: 'list-box', nombre: 'Objetivo', directTo: '/tabs/catalogo' }
  ]

}

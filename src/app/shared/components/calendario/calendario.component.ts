import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
/**
 * Componente que recibe una fecha y devuelve un calendadio con dia de semana, dia mes y mes.
 * @export
 * @class CalendarioComponent
 * @implements {OnChanges} recibe por parametros la fecha a devolver.
 */
@Component({
  selector: 'calendario',
  templateUrl: './calendario.component.html',
  styleUrls: ['./calendario.component.scss'],
})

export class CalendarioComponent implements OnChanges {
  /**
   *Fecha enviada por parametros
   * @type {string}
   * @memberof CalendarioComponent
   */
  @Input() fecha: string;

  /**
   * Dia del mes
   * @type {string}
   * @memberof CalendarioComponent
   */
  dia: string;

  /**
   * Dia de la semana tipo cadena
   * @type {string}
   * @memberof CalendarioComponent
   */
  semana: string;

  /**
   *
   *Mes de la fecha tipo cadena
   * @type {string}
   * @memberof CalendarioComponent
   */
  mes: string;

  /**
   * Contiene los dias de la semana
   *@type {Array}
   * @memberof CalendarioComponent
   */
  diaSemana = ['LUN', 'MAR', 'MIE', 'JUE', 'VIE', 'SAB', 'DOM'];

  /**
   * Contiene los meses del año
   *@type {Array}
   * @memberof CalendarioComponent
   */
  meses = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];

  /**
   *Creates an instance of CalendarioComponent.
   * @memberof CalendarioComponent
   */
  constructor() { }

  /**
   * Metodo que analiza si existe un cambio de variables en el componente.
   * @param {SimpleChanges} changes
   * @memberof CalendarioComponent
   */
  ngOnChanges(changes: SimpleChanges): void {

    this.setearFecha(this.fecha);
  }

  /**
   * Permite obtener dia mes y año de la fecha entregada por parametros.
   * @param {string} fecha
   * @memberof CalendarioComponent
   */
  setearFecha(fecha: string) {
    if (!fecha) {
      return
    }
    let fechaCorregida = fecha.split(" ");
    fechaCorregida = fechaCorregida[0].split("-")
    let fechaD = new Date(`${fechaCorregida[1]}-${fechaCorregida[0]}-${fechaCorregida[2]}`);
    this.semana = this.diaSemana[fechaD.getDay() - 1];
    this.dia = fechaCorregida[0];
    this.mes = this.meses[fechaD.getMonth()];

  }
}

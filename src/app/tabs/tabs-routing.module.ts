import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'inicio',
        children: [
          {
            path: '',
            loadChildren: () => import('../modules/zonal/zonal.module').then( m => m.ZonalModule)
          }
        ]
      },
      {
        path: 'vendedores',
        children: [
          {
            path: '',
            loadChildren: () =>import('../modules/vendedores/vendedores.module').then( m => m.VendedoresModule)
          }
        ]
      },
      /* {
        path: 'catalogo',
        children: [
          {
            path: '',
            loadChildren: () =>import('../modules/articulos/articulos.module').then( m => m.ArticulosModule)
          }
        ]
      }, */
      {
        path: '',
        redirectTo: '/tabs/inicio',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/inicio',
    pathMatch: 'full'
  },
 
 
  

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
